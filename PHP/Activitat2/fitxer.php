<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <title>Fitxer pujat</title>
    <link rel="stylesheet" href="./fitxers.css">
  </head>
  <body>
    <header>
      <h1>Fitxer pujat</h1>
    </header>
    <?php
    $file=file($_FILES['fitxer']["tmp_name"]);
    $fitxer=fopen('fitxers/fitxer.txt','w');
    foreach ($file as $nlin=>$linia){
      fwrite($fitxer,$linia."<br>");
    }
    fwrite($fitxer,"Text pujat el: ".date("d-m-y H:i:s")."<br>");
    fclose($fitxer);
    echo "El fitxer s'ha pujat correctament.<br><br>";
    echo "El fitxer conté: <br>";
    $file=file('fitxers/fitxer.txt');
    foreach ($file as $nlin=>$linia){
      echo $linia."<br>";
    }
    ?>
    <footer>
      <p>Ethan Escribá</p>
    </footer>
  </body>
</html>
