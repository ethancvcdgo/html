<!DOCTYPE html>
<html lang="ca">
  <head>
    <title>FormulariPHP</title>
    <meta charset="utf-8">
    <meta name="author" content="Ethan Escribá">
    <link rel="stylesheet" href="./form.css">
  </head>
  <body>
    <p>Nom: <?php echo $_POST['nom'];?></p>
    <p>Cognoms: <?php echo $_POST['cognoms'];?></p>
    <p>Edat: <?php echo $_POST['naixement'];?></p>
    <p>Població: <?php echo $_POST['poblacio'];?></p>
    <?php move_uploaded_file($_FILES["foto"]["tmp_name"],"imatgesform/".$_FILES["foto"]["name"]);?>
  </body>
</html>
