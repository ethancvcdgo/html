<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <title>Multiplicar</title>
    <link rel="stylesheet" href="./multiplicar.css">
  </head>
  <body>
    <header>
      <h1>Taula Multiplicar</h1>
    </header>
    <table>
    <?php
      $num=5;
      echo "<tr><th>";
      print_r("<p>Taula del ".$num."</p>");
      echo "</th></tr>";

      for($i=0;$i<=10;$i++){
        echo "<tr>";
        if ($i%2==0) {
          echo "<td class=\"parell\">";
          print_r($num."*".$i." = ".$num*$i);
        }else{
          echo "<td class=\"senar\">";
          print_r($num."*".$i." = ".$num*$i);
        }
        echo "</td>";
        echo "</tr>";
      }
    ?>
    </table>
    <footer>
      <p>Ethan Escribá</p>
    </footer>
  </body>
</html>
